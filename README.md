# 阿波罗配置中心laravel扩展包

[![Latest Stable Version](http://poser.pugx.org/lihuababa/apollo-client/v)](https://packagist.org/packages/lihuababa/apollo-client) 
[![Total Downloads](http://poser.pugx.org/lihuababa/apollo-client/downloads)](https://packagist.org/packages/lihuababa/apollo-client) 
[![Latest Unstable Version](http://poser.pugx.org/lihuababa/apollo-client/v/unstable)](https://packagist.org/packages/lihuababa/apollo-client) 
[![License](http://poser.pugx.org/lihuababa/apollo-client/license)](https://packagist.org/packages/lihuababa/apollo-client) 
[![PHP Version Require](http://poser.pugx.org/lihuababa/apollo-client/require/php)](https://packagist.org/packages/lihuababa/apollo-client)

# 介绍
apollo-client 是携程阿波罗PHP客户端，通过命令行执行，为了方便使用，如有任何问题请发送邮件至lihuababa@aliyun.com

# 要求
- php版本:>=7.4
- laravel版本: Laravel8+


# 安装

```php
composer require lihuababa/apollo-client
```

# 在laravel项目中使用

# 执行命令
！！！执行命令后，如果成功拉取到配置，则拉取到的配置会覆盖本地.env文件
```php
php artisan apollo-client:pull-config --server=http://127.0.0.1:8080/  --app_id=202211101001 --cluster=default --namespace=application --secret=**** --debug=true
```

### 命令参数说明
| 字段  | 说明                    |
|-----|-----------------------|
| server | 阿波罗配置中心主机             |
| app_id | APPID                 |
| cluster | 集群                    |
| namespace | 空间                    |
| secret | 密钥                    |
| debug | 是否debug，如果为真，则会一直更新配置 |
