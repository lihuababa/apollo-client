<?php
namespace ApolloClient;

use Illuminate\Support\ServiceProvider;

class ApolloClientServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $commands = [
        Console\ApolloPullConfig::class,
    ];
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }

}
